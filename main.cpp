#include "vials.h"

Vial_collection* decant(Vial_collection* _vials, const int target_dose) {
    // logics
	if (target_dose > 0 && target_dose < static_cast<int>(Liquid_dose::random)) {
		if (target_dose == 1) {
			Vial_set* copy = new Vial_set();
			_vials->get_set_copy(copy, 0);


		}
	}

	return _vials;
}

void print_decant(Vial_collection* _vials) {
    printf("\nDecanting results:\n");

	Vial_set* set = new Vial_set();
	for (int i = 0; i < _vials->get_count(); ++i) {
		_vials->get_set_copy(set, i);
		printf("Set %i/%i:\ttype = %s\tdose = %i\tsize = %i\n", i + 1, _vials->get_count(), liquid_name[set->get_type()].c_str(), static_cast<int>(set->get_dose()), set->get_size());
	}
	delete set;
}

int main(int argc, char* argv[]) {
	// Input

	// Random
	Vial_collection* inp_1 = new Vial_collection(10);
	while (!inp_1->is_full()) {
		Vial_set* set = new Vial_set(0, Liquid_type::any, Liquid_dose::random);
		inp_1->add_to_collection(set, nullptr);
	}

	// Fixed
	Vial_collection* inp_2 = new Vial_collection(5);
	Vial_set* set = new Vial_set(69, Liquid_type::acid, Liquid_dose::one);
	inp_2->add_to_collection(set, nullptr);
	set = new Vial_set(69, Liquid_type::acid, Liquid_dose::two);
	inp_2->add_to_collection(set, nullptr);
	set = new Vial_set(100, Liquid_type::juice, Liquid_dose::three);
	inp_2->add_to_collection(set, nullptr);

	// Process
    if(argc > 1) {
        int arg = static_cast<int>(*argv[1]);

		print_decant(decant(inp_1, arg));
		print_decant(decant(inp_2, arg));
    }

	// Clean
	inp_1->~Vial_collection();
	inp_2->~Vial_collection();
	getchar();
    return 0;
}