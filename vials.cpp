#include "vials.h"

#pragma region Vial_set

// Private
int Vial_set::gen_random(const int &min, const int &max) {
	std::mt19937 eng{ std::random_device{}() };
	std::uniform_int_distribution<> distr(min, max);
	return distr(eng);
}
void Vial_set::set_empty() {
	m_type = Liquid_type::empty;
	m_dose = Liquid_dose::zero;
}
// Public
Vial_set::Vial_set(int _size, Liquid_type _type, Liquid_dose _dose) {
	m_size = (_size <= 0) ? gen_random(1, INT_MAX) : _size;
	set_type(_type);
	set_dose(_dose);
}
Vial_set::Vial_set() {
	Vial_set(0, Liquid_type::empty, Liquid_dose::zero);
}
Vial_set::~Vial_set() {

}

// Setters with restrictions
void Vial_set::set_type(Liquid_type _type) {
	switch (_type) {
	case empty:
		set_empty();
		break;
	case any:
		m_type = static_cast<Liquid_type>(gen_random(0, _type - 1));
		if (m_type == Liquid_type::empty) set_empty();
		break;
	default:
		m_type = _type;
	}
}
void Vial_set::set_dose(Liquid_dose _dose) {
	switch (_dose) {
	case zero:
		set_empty();
		break;
	case random:
		m_dose = static_cast<Liquid_dose>(gen_random(0, _dose - 1));
		if (m_dose == Liquid_dose::random) set_empty();
		break;
	default:
		m_dose = _dose;
	}
}
void Vial_set::set_size(int _size) {
	(_size > 0) ? m_size = _size : m_size = 0;
}
int Vial_set::increase_size(int _amount) {
	if (_amount > 0) {
		if (m_size > INT_MAX - _amount) {
			int rem = _amount - (INT_MAX - m_size);
			m_size += _amount - rem;
			return rem;
		}
		m_size += _amount;
	}
	return 0;
}

// Getters
Liquid_type Vial_set::get_type() { return m_type; }
Liquid_dose Vial_set::get_dose() { return m_dose; }
int Vial_set::get_size() { return m_size; }
#pragma endregion


#pragma region Vial_collection

Vial_collection::Vial_collection(int _size) {
	if (_size <= 0) return;
	m_size = _size;
	m_count = 0;
	m_collection = new Vial_set*[m_size];

	// Explicitly set all set pointers to null
	Vial_set** iter = m_collection;
	for (int i = 0; i < m_size; ++i) {
		(*iter) = nullptr;
		++iter;
	}
}

Vial_collection::~Vial_collection() {
	Vial_set** iter = m_collection;
	for (int i = 0; i < m_count; ++i) {
		delete (*iter);
		++iter;
	}

	delete[] m_collection;
	m_collection = nullptr;
}

// Add new set and merge with existing when possible 
bool Vial_collection::add_to_collection(Vial_set* _set, Vial_set* _overflow_set) {
	if (_set != nullptr && m_count < m_size && _set->get_size() > 0) {
		for (int i = 0; i < m_count; ++i) {
			if (m_collection[i]->get_type() == _set->get_type() && m_collection[i]->get_dose() == _set->get_dose()) {
				int remainder = m_collection[i]->increase_size(_set->get_size());
				if (remainder == 0) return true;

				if (_overflow_set != nullptr) {
					_overflow_set->set_dose(_set->get_dose());
					_overflow_set->set_type(_set->get_type());
					_overflow_set->set_size(remainder);
				}
				return false;
			}
		}
		m_collection[m_count] = _set;
		++m_count;
		return true;
	}
	return false;
}

void Vial_collection::remove_from_collection(const int _index) {
	if (_index >= 0 && _index < m_count) {
		delete m_collection[_index];
		m_collection[_index] = nullptr;
		--m_count;

		// Push non-empty elements to the front
		Vial_set* iter = m_collection[_index];
		for (int i = 0; i < m_count - _index; ++i) {
			m_collection[_index + i] = m_collection[_index + i + 1];
			m_collection[_index + i + 1] = nullptr;
		}
	}
}

// Getters
bool Vial_collection::is_full() { return (m_count == m_size); }

int Vial_collection::get_count() { return m_count; }

void Vial_collection::get_set_copy(Vial_set* _copy, int _index) {
	if (_copy != nullptr && _index >= 0 && _index < m_count) {
		_copy->set_dose(m_collection[_index]->get_dose());
		_copy->set_type(m_collection[_index]->get_type());
		_copy->set_size(m_collection[_index]->get_size());
	}
}

// Setters
bool Vial_collection::set_from_copy(Vial_set* _set, int _index) {
	if (_set != nullptr && _index >= 0 && _index < m_count) {
		if (_set->get_size() == 0) {
			remove_from_collection(_index);
		}
		else {
			m_collection[_index]->set_size(_set->get_size());
			m_collection[_index]->set_dose(_set->get_dose());
			m_collection[_index]->set_type(_set->get_type());
		}
		return true;
	}
	return false;
}
#pragma endregion
