#ifndef _VIALS_H
#define _VIALS_H

#include <random>
#include <string>

const std::string liquid_name[] = { "empty", "water", "acid", "juice" };
enum Liquid_type {
	empty,
	water,
	acid,
	juice,
	any	// must be last
};
enum Liquid_dose {
	zero,	
	one,
	two,
	three,
	four,
	random	// must be last
};

class Vial_set {
private:
	Liquid_type m_type;
	Liquid_dose m_dose;
	int m_size;

	int gen_random(const int &min, const int &max);
	void set_empty();
public:
	Vial_set(int _size, Liquid_type _type, Liquid_dose _dose);
	Vial_set();

	~Vial_set();

	// Setters with restrictions
	void set_type(Liquid_type _type);
	void set_dose(Liquid_dose _dose);
	void set_size(int _size);

	// Returns a remainder if size overflows
	int increase_size(int _amount);

	// Getters
	Liquid_type get_type();
	Liquid_dose get_dose();
	int get_size();
};

class Vial_collection {
private:
	Vial_set** m_collection;
	int m_size;
	int m_count;
public:
	Vial_collection(int _size);

	~Vial_collection();

	// Add new set and merge with existing when possible 
	bool add_to_collection(Vial_set* _set, Vial_set* _overflow_set);

	void remove_from_collection(const int _index);

	// Getters
	bool is_full();
	int get_count();
	void get_set_copy(Vial_set* _copy, int _index);

	// Setters
	bool set_from_copy(Vial_set* _set, int _index);
};

#endif

